# README #

### What is this repository for? ###

This repository is for the Multimodel Emulation Project run by the Center for Robust Decision Making on Climate and Energy Policy (RDCEP).

### How do I get set up? ###

The emulator requires that two environment variables be set: 
 * Set R_LIBS to the directory where you want R packages installed.
 * Set CLIMATE_EMULATOR_REPO to the absolute path to this repository on your machine.

If you are using this repository as the backend to the FACE-IT emulator tool, you need to set these environment variables in each of the executable scripts in "climate_emulator/1.0.0/faceit_main_scripts" . 

### Who do I talk to? ###

If you have any questions about this repository, contact Aidan Sadowski at amsadowski5@gmail.com , or info.rdcep@uchicago.edu . 


### License ###

Copyright © 2016, The University of Chicago.  
All rights reserved.

Multimodel Climate Emulation Project, Version 1.0.0
OPEN SOURCE LICENSE

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the “Software”), to deal with 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

Redistributions of source code must retain the above copyright notice, this list of 
conditions and the following disclaimers. Software changes, modifications, or 
derivative works, should be noted with comments and the author and organization's name.

Redistributions in binary form must reproduce the above copyright notice, this list of 
conditions and the following disclaimers in the documentation and/or other materials 
provided with the distribution.

Neither the names of RDCEP, nor the Computation Institute, the University of Chicago, 
UChicago Argonne LLC, Argonne National Laboratory, the U.S. Government nor the names 
of their contributors may be used to endorse or promote products derived from this 
Software without specific prior written permission.

The software and the end-user documentation included with the redistribution, 
if any, must include the following acknowledgment: "This product includes 
software produced by the Center for Robust Decision-making on Climate and Energy 
Policy (RDCEP)." 

***************************************************************************************
DISCLAIMER

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  
NEITHER RDCEP, NOR THE UNIVERSITY OF CHICAGO, NOR THE UNITED STATES GOVERNMENT, 
NOR THE UNITED STATES DEPARTMENT OF ENERGY, NOR UCHICAGO ARGONNE, LLC, NOR ANY 
OF THEIR EMPLOYEES, MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LEGAL 
LIABILITY OR RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY 
INFORMATION, DATA, APPARATUS, PRODUCT, OR PROCESS DISCLOSED, OR REPRESENTS THAT
ITS USE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS. IN NO EVENT SHALL THE CONTRIBUTORS 
OR COPYRIGHT HOLDERS, BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.