

**** Emulator TODO ****

[.?] Visualization tools
[  ] Seasonal and max/min emulation
[  ] I_1 and I_2 index calculation tools *
[  ] GeoJSON region map creation tool 
[  ] Import more models into data library
[  ] Train parameters and verify quality for more models 

[  ] Montly data should be weighted by month length instead of treated as equal
[  ] New datatypes (as wrappers around .csv) for input and intermediate steps
[  ] Grid Scale (pixel level) option in region aggregation
[  ] % confidence intervals for mean temperature estimate
[  ] Additional data validation on user input (e.g. check not "None" which is the face-it auto-fill value if the user leaves a textbox blank, etc)



**** Map Region Selector Tool TODO ****

[  ] Doesn't work inside double conditional *
[  ] "Select all"/"deselect all" buttons
[  ] Select / Deselect in one click, not open menu
[  ] Hover provides region name / number
[  ] Remeber selections on "run this tool again"
[  ] Remove option to drag map infinitely
[  ] Choose better colors, one tester thought blue meant "selected" and that they were all selected by default
[  ] Turn down drag sensitivity
[  ] Currently the map sends the whole geojson file to the R script, but there might be a way to send just the selected region names
[  ] When users train their own parameters user their own geojson, the map tool should be able to use their geojson to let them select their regions on "Custom" parameter option *




**** Finished this Summer ****

General outline:
[*/] Tool for emulation
[*/] Tool for aggregating ncdf files into regions
[*/] Tool for training new parameters
[*/] Tool for visualizing emulator vs model 
[*/] User Guide / Documentation

Details:
[*/] User defined regions using geojson or csv mask
[*/] User uploaded ncdf file can be paired with any co2 data, no longer limited to RCPs
[*/] Accepts ncdf files at any time frequency, not just monthly
[*/] Global mean option 
[*/] Rounding
[*/] Plots can have R^2 value to model data - and csv download 
[*/] Add region full names (instead of just region numbers) to parameter output file when user uploads a mask defining named regions
[*/] Option to use our geopolitical regions map in the Region Aggregation tool
[*/] Correct region discrepencies in default mask
[*/] Put CMIP5 data in shared data library (done for 4 major models)
[*/] Update dataset of "best parameters"
