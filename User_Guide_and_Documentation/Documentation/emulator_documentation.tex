\documentclass{article}

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt,margin=1in]{geometry} % Document margins
\usepackage{multicol} % Used for the two-column layout of the document
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{float} % Required for tables and figures in the multi-column environment - they need to be placed in specific locations with the [H] (e.g. \begin{table}[H])
\usepackage{hyperref} % For hyperlinks in the PDF

\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text
\usepackage{paralist} % Used for the compactitem environment which makes bullet points with less space between them

\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\Roman{subsection}} % Roman numerals for subsections
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{\underline}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles

\usepackage{fancyhdr} % Headers and footers
%\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
%\fancyhead[C]{Running title $\bullet$ November 2012 $\bullet$ Vol. XXI, No. 1} % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{longtable} % Allows tables that break across pages
\usepackage{graphicx}

\setlength{\parindent}{4em}
\setlength{\parskip}{1em}


\title{User's Guide to the Climate Model Emulator}

\begin{document}

\maketitle

\tableofcontents



\section{Introduction}

This documents contains the implementation details of the emulator. If you just want to know what the emulator is for and how to use the face-it interface to the emulator, see the User Guide.



\section{Why You Would Use the Emulator}

The current most common method for predicting temperature change for a given CO$_2$ scenario is to use a General Circulation Model (GCM), a state-of-the-art climate model. These GCMs, however, are extremely computationally expensive and can take on the order of weeks or months to produce temperature predictions. This makes them infeasible for some studies on the impacts of climate change. For example, a study seeking to find an optimal carbon tax would want to calculate the economic damages from a wide range of CO$_2$ scenarios, each representing total emissions for a given value of the tax. Such a study could take years simply to produce the data if all the temperature projections were generated directly by a GCM.
\par
The emulator allows researchers to produce future mean temperature projections that closely match GCM behavior but on the order of seconds.
\par
The Climate Model Emulator is not the only method for producing temperature projections for a wide range of CO$_2$ scenarios quickly. For a comparison of emulation to other methods, see [Castuccio Paper]


\section{Code}

All code used in the Multimodel Emulation Project is available at \url{https://amsadowski@bitbucket.org/amsadowski/climate_emulator.git}  . 



\section{The Emulator Process}


\begin{figure}[H]
	\includegraphics[width=\textwidth]{process_cartoon_bigger_font}
	\caption{The two step process of emulation. (1) GCM output data (training set) is used to train parameters. (2) The parameters are used to predict model behavior for a user-supplied CO$_2$ scenario. (The Training Set image shows the temperature predictions of the climate model CCSM4 for each of the four RCP scenarios. The Emulated Temperature image shows the emulation (blue) plotted over the original climate model predictions (gray)).}
\end{figure}

The Climate Model Emulation process is divided into two steps: parameter training and emulation. 

The parameter training step begins by taking GCM predictions for future temperature for a given CO$_2$ scenario. The selected GCM predictions are known as the training set. We fit the parameters of the equation given in the section \textbf{Functional Form of the Emulator} to the data in the training set. More information can be found in the section \textbf{Fitting the Parameters}.

The emulation step begins once parameters are generated. The emulator takes the parameters and a CO$_2$ scenario and outputs approximately what the climate model would have predicted for mean temperature in each year.  




\section{Functional Form of the Emulator}

The emulator is an equation in the following form: 

\begin{equation}
T(t) = \beta_0 + \beta_{1}(1-\rho)\sum_{k=0}^{t-1} \rho^k \log (\frac{\mathrm{CO}_2[t-k]}{\mathrm{CO}_{2preindustrial}}) + AR(1)
\end{equation}

The equation takes in a year $t$ and outputs mean temperature for that year in Kelvin. We capture temperature variability and autoregressivity with a simple AR(1) model.

 
\section{Fitting the Parameters}

The fitting procedure finds the parameter values that minimize the sum of residual squares of $T(t)$ to the temperature in the training set.  

Let $DECAY(t, \rho) = (1-\rho)\sum_{k=0}^{t-1} \rho^k \log (\frac{\mathrm{CO}_2[t-k]}{\mathrm{CO}_{2preindustrial}})$. The emulation equation then becomes $T(t) = \beta_0 + \beta_1DECAY(t, \rho)$ + AR(1). For a fixed rho, this a linear equation relating $T(t)$ and $DECAY(t)$ with an autoregressive term and can be fit using the R function $\textit{arima()}$ 

The fitting procedure uses the R method $\textit{optim()}$ to iterate through values of $\rho$, then for a given candidate value of $\rho$ uses $\textit{arima()}$ to find the optimal $\beta_0$ and $\beta_1$ as described in the preceding paragraph. (The parameters of the AR(1) portion are not relevant to the mean emulation.) 


\section{Limitations of the Emulator and Disclaimers}

The emulator is meant for reproducing mean temperature. The simple representation of variability (an AR(1) model) may make it inappropriate for modeling future temperature variability. 

The emulator is best at predicting model behavior within a time scale of about a century. The parameterization does not distinguish between lags or forcing responses at different time scales which makes it inappropriate for predicting temperature for multi-centennial scale CO$_2$ scenarios where long term lags become relevant. Basically, don't try to emulate scenarios longer than 100 years. 


\section{Tables}
\begin{longtable}{l l l}
\caption{Regions used in the multimodel emulation project. Nonglobal regions contain either entirely land or entirely ocean. Regions 1-3 are globals, 4-40 are land, and 41-63 are ocean. }\\
Region \# & Abbr. & Name \\
\hline \\
1 & MDE & Middle East\\ 
2 & ANT & Antarctica \\ 
3 & BRA & Brazil\\ 
4 & CAE & Eastern Canada\\ 
5 & CNA & China\\ 
6 & SSA & Southern South America\\ 
7 & WSA & Western South America\\ 
8 & NSA & Northern South America\\ 
9 & CEA & Central America\\ 
10 & MEX & Mexico\\ 
11 & EUW & Western Europe\\ 
12 & MOG & Mongolia\\ 
13 & NOR & Nordic\\ 
14 & AUS & Australia\\ 
15 & KAZ & Kazak\\ 
16 & JAP & Japan\\ 
17 & RSE & Eastern Russia\\ 
18 & AIS & Asian Isles\\ 
19 & KOP & Korean Peninsula\\ 
20 & USE & Eastern United States\\ 
21 & SEA & Southeast Asia\\ 
22 & EUE & Eastern Europe\\ 
23 & SAL & South Africa and Lesotho\\ 
24 & SAF & Southern Africa\\ 
25 & MAD & Madagascar\\ 
26 & IND & India\\ 
27 & GRE & Greenland\\ 
28 & NAF & North Africa\\ 
29 & NWZ & New Zealand\\ 
30 & WAF & Western Africa\\ 
31 & CAF & Central Africa\\ 
32 & ENE & Northern Europe\\ 
33 & USW & Western United States\\ 
34 & CAW & Western Canada\\ 
35 & ALA & Alaska\\ 
36 & RSW & Western Russia\\ 
37 & ENG & England and Ireland\\ 
38 & MED & Mediterranean\\ 
39 & CAR & Caribbean\\ 
40 & INO & Indian Ocean\\ 
41 & SPW & South Western Pacific\\ 
42 & SPE & South Eastern Pacific\\ 
43 & EPW & Western Equatorial Pacific\\ 
44 & EPE & Eastern Equatorial Pacific\\ 
45 & NPW & North Western Pacific\\ 
46 & NPE & North Eastern Pacific\\ 
47 & NAT & Northern Atlantic\\ 
48 & SAT & Southern Atlantic\\ 
49 & EAT & Equatorial Atlantic\\ 
50 & SIO & Southern Indian Ocean\\ 
51 & NNE & Far North Eastern Pacific\\ 
52 & NNW & Far North Western Pacific\\ 
53 & WPE & Warm Pool East\\ 
54 & WPS & Warm Pool South\\ 
55 & HBO & Hudson Bay\\ 
56 & WPN & Warm Pool North\\ 
57 & NNA & Far Northern Atlantic\\ 
58 & ARO & Arctic Ocean\\ 
59 & AOP & Antarctic Ocean Pacific\\ 
60 & AOI & Antarctic Ocean Indian\\
61 & G & Global\\ 
62 & GL & Global Land\\ 
63 & GO & Global Ocean\\ 
\end{longtable}


\end{document}





