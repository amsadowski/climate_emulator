\documentclass{article}

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt,margin=1in]{geometry} % Document margins
\usepackage{multicol} % Used for the two-column layout of the document
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{float} % Required for tables and figures in the multi-column environment - they need to be placed in specific locations with the [H] (e.g. \begin{table}[H])
\usepackage{hyperref} % For hyperlinks in the PDF

\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text
\usepackage{paralist} % Used for the compactitem environment which makes bullet points with less space between them

\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\Roman{subsection}} % Roman numerals for subsections
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{\underline}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles

\usepackage{fancyhdr} % Headers and footers
%\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
%\fancyhead[C]{Running title $\bullet$ November 2012 $\bullet$ Vol. XXI, No. 1} % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{longtable} % Allows tables that break across pages
\usepackage{graphicx}

%indents
\usepackage{indentfirst}
\setlength{\parindent}{4em} %indentation of paragraph
\setlength{\parskip}{1em}
\newcommand{\itemspace}{0.1em} %spacing between items in a list


%% Define FACE-IT parameter names
\newcommand{\uploadOrPreset}{\textbf{Use standard IPCC specified CO2 scenario or use uploaded}}
\newcommand{\coScenario}{\textbf{CO$_2$ Scenario}}
\newcommand{\uploadOrPrecalc}{\textbf{Use precalculated model parameters or custom parameters}}
\newcommand{\model}{\textbf{Model}}
\newcommand{\regions}{\textbf{Regions...}}
\newcommand{\pfile}{\textbf{Parameter File}}
\newcommand{\anom}{\textbf{Output temperature as change from the starting year instead of absolute temperature}}
\newcommand{\mapOrText}{\textbf{Select regions using the map or using textbox input}}

\newcommand{\contact}{\textit{info.rdcep@uchicago.edu}}


\title{User's Guide to the Climate Model Emulator}

\begin{document}

\maketitle

\tableofcontents


\section{Introduction}

The Climate Model Emulator is a tool that mimics climate model predictions for future temperature. It simulates state-of-the-art climate models known as General Circulation Models (GCMs), giving approximately the same predictions that these advanced models would have given - without the computational expense of running the models themselves.

The user inputs a trajectory of future CO$_2$ concentrations known as a CO$_2$ scenario and the emulator outputs approximate model predictions for mean temperature. 

Researchers can use the emulator to generate projections of future mean temperature in place of using GCMs directly.

\begin{figure}[H] %emulation of two scenarios of CCSM4 to visually show quality of fit of emulation
	\centering
	\includegraphics[width=0.8\textwidth]{ccsm4emulation_sametset}
	\caption{Climate Model predictions for change in temperature (black) plotted behind the output of the emulator (blue). The left is model output for RCP 3.0 and the right is for RCP 6.0 . The emulator was trained on model output for RCP 4.5 and RCP 8.5. The climate model is CCSM4 and the region is Western United States. The emulator accurately predicts model mean temperature output and could be used as a way to generate temperature data for a wide range of CO$_2$ scenarios.}
\end{figure}


\section{Why You Would Use the Emulator}

The current most common method for predicting temperature change for a given CO$_2$ scenario is to use a General Circulation Model (GCM), a complex representation of the Earth's climate system. These GCMs, however, are extremely computationally expensive and can take on the order of weeks or months to produce temperature predictions. This makes it infeasible to use them to produce temperature predictions for studies on the impacts of climate change that need predictions over a wide range of CO$_2$ scenarios. For example, a study seeking to find an optimal carbon tax would want to calculate the economic damages of the carbon emissions from a wide range of values of the tax. Such a study could take months simply to produce the data if all the temperature projections were generated directly by a GCM.
\par
The emulator allows researchers to produce future mean temperature projections that closely match GCM behavior but on the order of seconds.
\par
The Climate Model Emulator is not the only method for producing temperature projections for a wide range of CO$_2$ scenarios quickly. For a comparison of emulation to other methods, and for more technical details on the emulation, see [1].



\section{The Emulator Process}


\begin{figure}[H]
	\includegraphics[width=\textwidth]{process_cartoon_bigger_font}
	\caption{The two step process of emulation. (1) GCM output data (training set) is used to train parameters. (2) The parameters are used to predict model behavior for a user-supplied CO$_2$ scenario. (The Emulated Temperature image shows the emulation (blue) plotted over the original climate model predictions (gray)).}
\end{figure}

The Climate Model Emulation process is divided into two steps: parameter training and emulation. 

The parameter training step begins by taking GCM predictions for future temperature for a given CO$_2$ scenario. The selected GCM predictions are known as the training set. We fit the parameters of the equation given in the section \textbf{Functional Form of the Emulator} to the data in the training set. More information can be found in the Emulator Documentation.

The emulation step begins after parameters are generated. The emulator takes the parameters and a CO$_2$ scenario and outputs approximately what the climate model would have predicted for mean temperature in each year. 

 



\section{I want to...}


\subsection{import climate model data into my face-it history.}

Face-it contains a large repository of climate model Temperature at Surface (tas) data which can be used to create training sets for the emulator. GCMs output temperature data in NCDF files. To access these, follow the guide below. 

1. Click \textbf{Shared Data} in the top bar. This will take you to the Shared Data Library menu.

2. Click \textbf{GCM Temperature at Surface} to access the data library for temperature data.

3. Data is sorted into folders by the name of the climate model that produced them. Click the blue arrow to the left of the folder icon to expand it. 

If you want data for a model that is unavailable in the Shared Data Library, you can contact \contact\ to request that we add new data.

Each file has a naming convention to identify it. The filename convention is ``tas\_[time frequency]\_[model name]\_[co2 scenario]\_r[realization number]\_i1\_p1\_[start date]-[end date].nc''. 

For example, the file name ``tas\_Amon\_CCSM4\_rcp45\_r1i1p1\_200601-210012.nc'' contains data that is Monthly (Amon), produced by the model CCSM4, running CO$_2$ scenario RCP 4.5, realization 1, start year 2006. 
Select all NCDF files you wish to use by clicking the checkbox next to the file name. 

4. Scroll down to the bottom and set  ``For selected datasets'' to \textit{Import to current history} and click ``Go''. If you have multiple histories, you will then be asked to select a history.

Your data should now be available in your history to use as the input to the \textbf{Aggregate temperature data into regions} tool. 


\subsection{predict future temperature for a climate model...}
	
	\newcommand{\ScenarioCommonHead}	
	{
		\par
		1. Find and click the ``Emulator'' toolshed on the left hand side. Click the Emulate tool.
	}
	
	\newcommand{\ScenarioCommonTail}
	{
		\par
		3. Choose whether you want each region to output absolute temperature, or change in temperature from the starting year. If you want change in temperature, click the box for \anom. If you want absolute, leave it blank.
		\par
		4. Choose whether to use precalculated parameters or to use your own. If you want to emulate the behavior of a standard climate model, set \uploadOrPrecalc\ to \textit{Precalculated Model}, then set \model\ to the model you want to emulate. The emulator will take the selected CO$_2$ scenario and output the emulation of the mean temperature prediction of your selected model. If you want to use your own parameters (because you want to emulate a model for which we do not have parameters available, or because you want to define your own regions) set \uploadOrPrecalc\ to \textit{Custom} and see the section for training new parameters or new regions below. You can also contract \contact\ to request that we add parameters for a model to our dataset or to request that we upload new data to the Shared Data Library.
		\par
		5. Select which regions of the world for which you want to predict future mean temperature. You can select regions in one of two ways.
		\par
		The first is to use the map selector tool. To select a region using the map select tool, click the region, then click ``I forget what the box says'' in the pop-up box. All regions are unselected by default.
		\par
		The second is to use text input. Each region of the world is assigned a number, given in table 1 at the end of this guide. To use the textbox, set \mapOrText\ to \textit{textbox}. You can enter the numbers of regions you wish to enter as a comma separated list. Ranges of regions can be shortened using ``:'', for example ``4,5,6,7,8'' can be shortened to ``4:8'', and ``4,5,6,7,8,20,21,21,23,30,40,41,42,43'' can be shortened to ``4:8,20:23,30,40:43''. This input mode allows you to quickly input regions such as all land regions (4:40) or all ocean regions (41:63), or to select continents like all of Asia (4,8,15,18:22,24,29).
		\par
		Note: regions are either all land or all ocean. 
		\par
		6. Click ``Execute'' to run the emulation.
	}
	
	
	\subsubsection{using one of the standard IPCC specified CO$_2$ scenarios.}
	
		\ScenarioCommonHead
	
		\par
		2. To use an IPCC scenario, set \uploadOrPreset \ to \textit{Standard}. You will then be able to select the desired scenario in \coScenario. 
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.5\textwidth]{rcp_scenarios}
			\caption{CO$_2$ equivalent concentrations for the four Representative Concentration Pathways (RCPs)}
		\end{figure}

		The IPCC specifies a number of scenarios that are considered to be realistic possibilities of future CO$_2$ known as the Representative Concentration Pathways (RCPs). These are depicted in figure [\#] below. RCP 2.6 represents a future where the world decreases emissions to zero and reduces CO$_2$ in the atmosphere. RCP 8.5 represents a future where emissions continue to increase as normal, a ``business as usual'' scenario. RCP 4.5 and 6.0 represent futures with moderate emissions reduction. [citation]
		\par
		The 5th scenario listed in \coScenario\ is \textit{Abrupt 4x}, a scenario used by researchers but that does not represent a possible future. It instantaneously increases the CO$_2$ concentration in the atmosphere to 1112 parts per million, 4 times the estimated preindustrial CO$_2$.
		\par
		
		\ScenarioCommonTail
	
	
	\subsubsection{using a CO$_2$ scenario that I upload.}
	
		\ScenarioCommonHead
		
		\par
		To use your own scenario, set \uploadOrPreset\ to \textit{Uploaded}. Then, set \coScenario\ to the dataset containing your CO$_2$ scenario. The file should be a csv and must contain two columns: ``year'' and ``co2'', Where ``year'' is the absolute year and ``co2'' is the concentration of CO$_2$ equivalent in the atmosphere in parts per million (ppm).  An example CO$_2$ scenario is given below:

		\begin{tabular}{l l}
			year, & co2   \\
			1850, & 281  \\
			1851, & 287  \\
			... &              \\
			2100, & 1148 \\
		\end{tabular}
	
Your file can have any range of years, but it must have a value for each year and the years must be given in order. The values for ``co2'' are really for CO$_2$ \textit{equivalent}, which is used to reduce all the various types of greenhouse gases into a common unit. The concentration of CO$_2$  \textit{equivalent} is the amount of CO$_2$ that would have the same climate change effect as the total amount of greenhouse gases. For reference, the CO$_2$ equivalent for 2013 is 478 ppm [http://oceans.mit.edu/news/featured-stories/5-questions-mits-ron-prinn-400-ppm-threshold].


	3-6: See steps 3-6 in the section ``Using one of the IPCC specified CO2 scenarios'' above.
	
	
	


\subsection{train new parameters using GCM data...}

\subsubsection{using the face-it webtool for training parameters.}

1. Find and click the ``Emulator'' toolshed on the left hand side. Click the Train Parameters Tool.

2. the Train Parameters tool takes in any number of ``training set items'', each of which is a csv that gives CO$_2$ and model temperature prediction for each year. These training set items are output by the \textbf{Aggregate temperature data into regions} tool described in the \textbf{define my own regions} section. The easiest way to train new parameters is to use the \textbf{Aggregate temperature data into regions} tool on NCDF files from the face-it shared data libraries to create training set items. 

If you want to define your own regions of the world, go to the section \textbf{define my own regions and predict temperature for those regions} below. If you want to use the emulation project default regions (listed in table 1 at the end of this document), follow steps 3-12 for each NCDF file:

3. Click the \textbf{Aggregate temperature data into regions} tool.

4. Set \textbf{NCDF file containing temperature at each grid cell} to the NCDF file.

5. Set \textbf{Use fault regions or upload your own map} to \textit{default}.

6. Click the checkbox for \textbf{Also calculate global mean temperature} if you want to calculate global mean temperature. This will be output as an additional region named ``Global''.

7. Enter into the textbox for \textbf{Model name} the name of the model that produced the NCDF file.

8. The NCDF file may be a model run for one of the standard IPCC specified runs (the RCPs and Abrupt 4x). If it is, set \textbf{Use standard IPCC specified CO2 scenario or use uploaded} to \textit{Standard}. Otherwise, you will have to set it to \textit{Uploaded} and upload a csv that gives the CO$_2$ in each year for the run that produced the NCDF data. The CO$_2$ file format is described in the subsection \textbf{using a CO$_2$ scenario that I upload.} 

9. If you are using a standard run, set the field \textbf{CO2 Scenario used to produce this temperature data} to the scenario name. Otherwise, set \textbf{CO2 scenario} to your uploaded CO$_2$ file and enter a name for the scenario in \textbf{CO2 name}.

10. Enter into the textbox for \textbf{First year in the data} the first year for which the NCDF file contains data. 

11. Enter into the textbox for \textbf{Realization} the realization number for the run that produced the NCDF file. 

If you retrieved your data from the \textbf{import climate model data into my faceit history} guide above, the information for steps 10-14 can be found in the filename of the NCDF file. Follow the convention given in that guide.

12. Click Execute. Remember that you must do this separately for each NCDF file. 

You can also, create your own training set items using your own software and upload them to face-it. Each item should be a csv with the following columns: 
\begin{itemize}
\setlength\itemsep{\itemspace}
\item time: the year of data in this row
\item co2: the co2 equivalent concentration for the given year in ppm
\item temp: the temperature for this year in kelvin
\item region: the number of the region for this row
\item model: the model that produced this item
\item scenario: the name of the co2 scenario that produced this item
\item realization: the number of the realization of this item (model runs started with different initial conditions are given different ``realization'' numbers)
\end{itemize}

Your data must be yearly. Each training set item should contain information for only a single model, scenario, and realization. 

This is the same data format as is produced by the \textbf{Aggregate temperature data into regions} tool.

13. The Train parameters tool allows you to upload any number of training set items. Click ``Add new Training Set Item'' for each one you wish to include in the training set. Set the value of the selection box to one of your items for each Training Set Item field.

15. Give your training set a name. the parameter file will contain the name of your training set so it can be distinguished from other parameters. A good convention is to include the names of all CO$_2$ scenarios and the number of unique realizations concatenated with ``\_''. For example, a training set with one realization each of RCP 4.5 and RCP 8.5 might be named ``45\_85\_r1''. 

16. Click ``Execute'' to train parameters using this training set.


\subsubsection{using my own parameter training software, then upload the parameter file.}

See the Emulator Documentation for the functional form of the emulator and the meaning of each parameter.

1. Upload your parameter file. It should be a csv with the following columns:

\begin{itemize}
\setlength\itemsep{\itemspace}
\item region: the number of the region which the parameters in this row are for
\item beta0: the value of $\beta_0$ for this region
\item beta1: the value of $\beta_1$ for this region
\item rho: the value of $\rho$ for this region
\item model: the name of the model which the parameters in this row are for
\item training\_set: the name of the training set that produced these parameters
\end{itemize}

You can have number of models, training sets, and regions in your parameter file. An example parameter file is given below:

	\begin{tabular}{l l l l l l}
	region, & beta0, & beta1, & rho, & model, & training\_set\\
	1, & 287.99, & 3.456, & 0.9835, & CCSM4, & 45\_60\\
	2, & 265.20, & 4.523, & 0.7645, & CCSM4, & 45\_60\\
	...
	\end{tabular}
	 

2. Follow the steps from \textbf{Predict future temperature for a climate model...} as normal, but in step 4, set \uploadOrPrecalc\ to ``Custom'' and set the \pfile\ selector box to your uploaded parameter file.



\subsection{define my own regions and predict temperature for those regions.}

GCMs divide the world into a grid and calculate temperature for each cell in the grid. The \textbf{Aggregate temperature data into regions} tool takes a NCDF file containing grid-level temperature data output from a GCM and turns it into yearly average temperatures for regions of the world. 

You can find NCDF files containing temperature data in the Shared Data Libraries on face-it. See the section \textbf{import climate model data into my face-it history} for how to access the shared data libraries. This data was retrieved from the Coupled Model Intercomparison Project Phase 5 (CMIP5) archive. You may wish to retrieve additional climate model temperature predictions from this archive and then upload them to face-it. Information about the CMIP5 archive can be found at \url{http://cmip-pcmdi.llnl.gov/cmip5/index.html}. 

1. Create a geoJSON file that defines the boundaries of your desired regions. You can easily create or modify a geoJSON file at \url{http://geojson.io/}. You should give each region the property ``name'' and set the value of the ``name'' property to a descriptive name of the region. This name will we used to identify the regions in the output from the \textbf{Aggregate temperature data into regions} tool. 

Alternatively, you can make a csv mask to define your regions, where the number in each cell represents the region to which that cell belongs. The map should be oriented such that lines of longitude run left to right, the north pole is the rightmost column, and the Greenwhich Meridian is the topmost row, as shown in the image below. It is difficult to align cell locations to real world borders so we strongly recommend that if you chose to use a csv map that you do not make it by hand.

\begin{figure}[H]
	\includegraphics[width=0.5\textwidth]{example_csv}
\end{figure}

2. Upload your geoJSON file or csv mask.

3. Import all NCDF files containing gridded Temperature at Surface data that you wish to aggregate into regions into your history. Again, see \textbf{import climate model data into my face-it history} for more information. 

4. Follow steps 5-15 for each NCDF file:

5. Set \textbf{NCDF file containing temperature at each grid cell} to the NCDF file.

6. Set \textbf{Use fault regions or upload your own map} to \textit{upload}.

7. Select if you are using a csv or geoJSON file to define regions using \textbf{Map file format}.

8. Set \textbf{Map of regions} to your map file.

9. Click the checkbox for \textbf{Also calculate global mean temperature} if you want to calculate global mean temperature. This will be output as an additional region named ``Global''.

10. Enter into the textbox for \textbf{Model name} the name of the model that produced the NCDF file.

11. The NCDF file may be a model run for one of the standard IPCC specified runs (the RCPs and Abrupt 4x). If it is, set \textbf{Use standard IPCC specified CO2 scenario or use uploadedo} to \textit{Standard}. Otherwise, you will have to set it to \textit{uploaded} and upload a csv that gives the CO$_2$ in each year for the run that produced the NCDF data. The CO$_2$ file format is described in the subsection \textbf{using a CO$_2$ scenario that I upload.} 

12. If you are using a standard run, set the field \textbf{CO2 Scenario used to produce this temperature data} to the scenario name. Otherwise, set \textbf{CO2 scenario} to your uploaded CO$_2$ file and enter a name for the scenario in \textbf{CO2 name}.

13. Enter into the textbox for \textbf{First year in the data} the first year for which the NCDF file contains data. 

14. Enter into the textbox for \textbf{Realization} the realization number for the run that produced the NCDF file. 

If you retrieved your data from the \textbf{import climate model data into my faceit history} guide above, the information for steps 10-14 can be found in the filename of the NCDF file. Follow the convention given in that guide.

15. Click Execute. Remember that you must do this separately for each NCDF file. 



\subsection{compare emulated temperature to the original climate model output.}

You can compare emulated temperature to model output using the \textbf{Visualize: Emulator vs Model} tool. This tool produces a plot that overlays the emulated temperature with the model temperature, similar to figure 1. 

1. Set \textbf{Emulator temperature data} to the dataset containing your emulated temperature. This will most likely be the output from the \textbf{Emulate} tool. 

2. Set \textbf{Model temperature data} to the dataset containing region-aggregated model temperature. You will most likely have to first use the \textbf{Aggregate temperature data into regions} tool described in an earlier section to region aggregate the model data. 

3. Select which regions for which you would like to produce a plot by entering the numbers of the regions in the textbox for \textbf{Regions to compare}.  You should enter the numbers as a comma separated list.

4. Select whether to show informative titles above the plot. The titles contain the region number, region name, scenario name, and $R^2$ value as a measure of the goodness of emulation. Uncheck the box under \textbf{Show titles} to disable titles.

Note that the $R^2$ value is a problematic measurement of the goodness of emulation. If the model data is has high variability, then $R^2$ will be small even when the emulator accurately predicts the mean temperature trend. The $R^2$ measurement will eventually be replaced with the I$_1$ index.

5. Select whether to plot all years in the data or just a subset of years. To plot all years, set \textbf{Limit range of years to plot} to \textit{No, plot all years}. To limit the range, set it to \textit{Yes, limit range} and fill in the textbox for \textbf{Lower bound year} and \textbf{Upper bound year} to the lowest and highest year you wish to appear in the plot, respectively. You might want to do this if the CO$_2$ scenario you emulated is longer than the model data, e.g. if you are using the RCPs.

6. Click Execute. This tool produces two outputs: a pdf file for the plots and a csv file for the $R^2$ value by region. One plot will be drawn for each region and all plots are placed onto a separate page in the pdf. 




\section{Tables}
\begin{longtable}{l l l}
\caption{Regions used in the multimodel emulation project. Nonglobal regions contain either entirely land or entirely ocean. Regions 1-3 are globals, 4-40 are land, and 41-63 are ocean. }\\
Region \# & Abbr. & Name \\
\hline \\
1 & MDE & Middle East\\ 
2 & ANT & Antarctica \\ 
3 & BRA & Brazil\\ 
4 & CAE & Eastern Canada\\ 
5 & CNA & China\\ 
6 & SSA & Southern South America\\ 
7 & WSA & Western South America\\ 
8 & NSA & Northern South America\\ 
9 & CEA & Central America\\ 
10 & MEX & Mexico\\ 
11 & EUW & Western Europe\\ 
12 & MOG & Mongolia\\ 
13 & NOR & Nordic\\ 
14 & AUS & Australia\\ 
15 & KAZ & Kazak\\ 
16 & JAP & Japan\\ 
17 & RSE & Eastern Russia\\ 
18 & AIS & Asian Isles\\ 
19 & KOP & Korean Peninsula\\ 
20 & USE & Eastern United States\\ 
21 & SEA & Southeast Asia\\ 
22 & EUE & Eastern Europe\\ 
23 & SAL & South Africa and Lesotho\\ 
24 & SAF & Southern Africa\\ 
25 & MAD & Madagascar\\ 
26 & IND & India\\ 
27 & GRE & Greenland\\ 
28 & NAF & North Africa\\ 
29 & NWZ & New Zealand\\ 
30 & WAF & Western Africa\\ 
31 & CAF & Central Africa\\ 
32 & ENE & Northern Europe\\ 
33 & USW & Western United States\\ 
34 & CAW & Western Canada\\ 
35 & ALA & Alaska\\ 
36 & RSW & Western Russia\\ 
37 & ENG & England and Ireland\\ 
38 & MED & Mediterranean\\ 
39 & CAR & Caribbean\\ 
40 & INO & Indian Ocean\\ 
41 & SPW & South Western Pacific\\ 
42 & SPE & South Eastern Pacific\\ 
43 & EPW & Western Equatorial Pacific\\ 
44 & EPE & Eastern Equatorial Pacific\\ 
45 & NPW & North Western Pacific\\ 
46 & NPE & North Eastern Pacific\\ 
47 & NAT & Northern Atlantic\\ 
48 & SAT & Southern Atlantic\\ 
49 & EAT & Equatorial Atlantic\\ 
50 & SIO & Southern Indian Ocean\\ 
51 & NNE & Far North Eastern Pacific\\ 
52 & NNW & Far North Western Pacific\\ 
53 & WPE & Warm Pool East\\ 
54 & WPS & Warm Pool South\\ 
55 & HBO & Hudson Bay\\ 
56 & WPN & Warm Pool North\\ 
57 & NNA & Far Northern Atlantic\\ 
58 & ARO & Arctic Ocean\\ 
59 & AOP & Antarctic Ocean Pacific\\ 
60 & AOI & Antarctic Ocean Indian\\
61 & G & Global\\ 
62 & GL & Global Land\\ 
63 & GO & Global Ocean\\ 
\end{longtable}


\section{Citations}
[1] Castruccio, S., McInerney, D.J., Stein, M.L., Liu, F., Jacob, R.J., Moyer, E.J. (2014), Statistical Emulation of Climate Model Projections Based on Precomputed GCM Runs, Journal of Climate, 27, 1829-1844.

We acknowledge the World Climate Research Programme's Working Group on Coupled Modelling, which is responsible for CMIP, and we thank the climate modeling groups for producing and making available their model output. For CMIP the U.S. Department of Energy's Program for Climate Model Diagnosis and Intercomparison provides coordinating support and led development of software infrastructure in partnership with the Global Organization for Earth System Science Portals.


\section{Contact}

If you have any questions about the Emulator, or you wish to request new features or data, contact \contact.

\end{document}




