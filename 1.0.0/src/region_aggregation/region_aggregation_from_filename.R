#######################################################################
#######################################################################
#####                                                             #####
#####           ██████╗ ██████╗  ██████╗███████╗██████╗           #####
#####           ██╔══██╗██╔══██╗██╔════╝██╔════╝██╔══██╗          ##### 
#####           ██████╔╝██║  ██║██║     █████╗  ██████╔╝          ##### 
#####           ██╔══██╗██║  ██║██║     ██╔══╝  ██╔═══╝           ##### 
#####           ██║  ██║██████╔╝╚██████╗███████╗██║               ##### 
#####           ╚═╝  ╚═╝╚═════╝  ╚═════╝╚══════╝╚═╝               ##### 
#####                                                             #####
#####              region_aggregation_from_filename.R             #####
#####                                                             #####
#####   Functions for taking an ncdf containing model             #####
#####   grid-resolution data and aggregating it into regions.     #####
#####   Read the WARNING below. These are legacy functions and    #####
#####   might not generalize to ncdf files other than CMIP5       #####
#####   RCP experiments for temperature at surface.               #####
#####                                                             #####
#####                                                             #####
#####                                                             #####
#####         Copyright © 2016, The University of Chicago.        #####
#####         All rights reserved.                                #####
#####                                                             #####
#####   Multimodel Climate Emulation Project, Version 1.0.0       #####
#####   OPEN SOURCE LICENSE                                       #####
#####                                                             #####
#####   Permission is hereby granted, free of charge, to any      #####
#####   person obtaining a copy of this software and associated   #####
#####   documentation files (the “Software”), to deal with the    #####
#####   Software without restriction, including without           #####
#####   limitation the rights to use, copy, modify, merge,        #####
#####   publish, distribute, sublicense, and/or sell copies of    #####
#####   the Software, and to permit persons to whom the Software  #####
#####   is furnished to do so, subject to the following           #####
#####   conditions:                                               #####
#####                                                             #####
#####   Redistributions of source code must retain the above      #####
#####   copyright notice, this list of conditions and the         #####
#####   following disclaimers. Software changes, modifications,   #####
#####   or derivative works, should be noted with comments and    #####
#####   the author and organization's name.                       #####
#####                                                             #####
#####   Redistributions in binary form must reproduce the above   #####
#####   copyright notice, this list of conditions and the         #####
#####   following disclaimers in the documentation and/or other   #####
#####   materials provided with the distribution.                 #####
#####                                                             #####
#####   Neither the names of RDCEP, nor the Computation           #####
#####   Institute, the University of Chicago, UChicago Argonne    #####
#####   LLC, Argonne National Laboratory, the U.S. Government     #####
#####   nor the names of their contributors may be used to        #####
#####   endorse or promote products derived from this Software    #####
#####   without specific prior written permission.                #####
#####                                                             #####
#####   The software and the end-user documentation included      #####
#####   with the redistribution, if any, must include the         #####
#####   following acknowledgment: "This product includes          #####
#####   software produced by the Center for Robust                #####
#####   Decision-making on Climate and Energy Policy (RDCEP)."    #####
#####                                                             #####
#####   *********************************************             #####
#####   DISCLAIMER                                                #####
#####                                                             #####
#####   THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF     #####
#####   ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED   #####
#####   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       #####
#####   PARTICULAR PURPOSE AND NONINFRINGEMENT.                   #####
#####   NEITHER RDCEP, NOR THE UNIVERSITY OF CHICAGO, NOR THE     #####
#####   UNITED STATES GOVERNMENT, NOR THE UNITED STATES           #####
#####   DEPARTMENT OF ENERGY, NOR UCHICAGO ARGONNE, LLC, NOR      #####
#####   ANY OF THEIR EMPLOYEES, MAKES ANY WARRANTY, EXPRESS OR    #####
#####   IMPLIED, OR ASSUMES ANY LEGAL LIABILITY OR                #####
#####   RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, OR         #####
#####   USEFULNESS OF ANY INFORMATION, DATA, APPARATUS, PRODUCT,  #####
#####   OR PROCESS DISCLOSED, OR REPRESENTS THAT ITS USE WOULD    #####
#####   NOT INFRINGE PRIVATELY OWNED RIGHTS. IN NO EVENT SHALL    #####
#####   THE CONTRIBUTORS OR COPYRIGHT HOLDERS, BE LIABLE FOR ANY  #####
#####   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   #####
#####   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR   #####
#####   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       #####
#####   DEALINGS WITH THE SOFTWARE.                               #####
#####                                                             #####
#######################################################################
#######################################################################

########################    WARNING    ##########################
# Use of this function is NOT ADVISED FOR USE IN ANYTHING INVOLVING
# EXTERNAL USER INPUT. The user may have changed filenames of downloaded
# ncdf data. Furthermore, it was only designed to work for the RCPs, 
# historical, and abrupt4xco2 experiments. It has not been tested on
# other experiments. This function is intended only for internal use
# when the researcher can be certain that the filenames of ncdf files
# follows the standard CMIP5 convention. 
####################################################################



#' get_info_from_filename_CMIP5_format
#'
#' Files in the CMIP5 archive follow a naming convention that allows us
#' to determine the model, scenario, realization, timestep, and starting
#' year of the run. This function extracts that information and returns it
#' in a list wth the following elements:
#'     * model:       the model
#'     * scenario:    the name of the co2 scenario 
#'     * co2:         the actual co2 values for the scenario
#'     * realization: the realization number of the run
#'     * start_year:  the year of the first data entry in the ncdf file
#'
#' @param file The filename WITHOUT the path.
get_info_from_filename_CMIP5_format <- function(file){
    model <- character()
    scenario <- numeric()
    realization <- numeric()
    start_year <- numeric()

    #tokenize
    split = strsplit(file, "_", fixed=TRUE)
    
    #if the scenario is "rcp", we just want everything after the p
    strsplit(split[[1]][4], "p")

    model = split[[1]][3]

    #this will either find a sequence of numbers in (30,45,60,85),
    #or it won't, in which case we match against the two experiments
    #for which we have yearly co2 data, historical and abrupt4xc02
    scenario = str_extract(split[[1]][4], "[0-9]+")[[1]]
    if(is.na(scenario)) {
        if(split[[1]][4] == "historical")
            scenario = 0
        else if(split[[1]][4] == "abrupt4xCO2")
            scenario = 4
        else
            scenario = -1 #we don't recognize
    }

    scenario = as.numeric(scenario)
    co2 = load_co2_file(scenario)
    realization = as.numeric(str_extract(str_extract(split[[1]][5], "r[0-9]+"), "[0-9]+"))
    start_year = as.numeric(str_extract(split[[1]][6], "[0-9]{4}"))


    list(model = model, scenario = scenario, co2 = co2,
        realization = realization, start_year = start_year)
}

aggregate_region_derive_from_filename <- function(ncdf, mask, var)
{
    timestep = get_timestep_CMIP5_format(ncdf)
    run_info = get_info_from_filename_CMIP5_format(ncdf)

    aggregate_region(ncdf, mask, run_info$model, run_info$co2, run_info$scenario,
                     run_info$start_year, run_info$realization, var)
}


aggregate_region_derive_from_filename_temperature <- function(ncdf, mask)
{
    aggregate_region_derive_from_filename(ncdf, mask, "tas")
}
