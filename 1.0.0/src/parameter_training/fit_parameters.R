#######################################################################
#######################################################################
#####                                                             #####
#####           ██████╗ ██████╗  ██████╗███████╗██████╗           #####
#####           ██╔══██╗██╔══██╗██╔════╝██╔════╝██╔══██╗          ##### 
#####           ██████╔╝██║  ██║██║     █████╗  ██████╔╝          ##### 
#####           ██╔══██╗██║  ██║██║     ██╔══╝  ██╔═══╝           ##### 
#####           ██║  ██║██████╔╝╚██████╗███████╗██║               ##### 
#####           ╚═╝  ╚═╝╚═════╝  ╚═════╝╚══════╝╚═╝               ##### 
#####                                                             #####
#####                       fit_parameters.R                      #####
#####                                                             #####
#####   Functions for fitting the parameters of the emulation     #####
#####   equation using data passed in by the user. This code      #####
#####   was originally produced by Feifei Liu Crouch in 2014      #####
#####   and contained no documentation. Documentation and         #####
#####   modifications were added by Aidan Sadowski.               #####
#####                                                             #####
#####                                                             #####
#####                                                             #####
#####         Copyright © 2016, The University of Chicago.        #####
#####         All rights reserved.                                #####
#####                                                             #####
#####   Multimodel Climate Emulation Project, Version 1.0.0       #####
#####   OPEN SOURCE LICENSE                                       #####
#####                                                             #####
#####   Permission is hereby granted, free of charge, to any      #####
#####   person obtaining a copy of this software and associated   #####
#####   documentation files (the “Software”), to deal with the    #####
#####   Software without restriction, including without           #####
#####   limitation the rights to use, copy, modify, merge,        #####
#####   publish, distribute, sublicense, and/or sell copies of    #####
#####   the Software, and to permit persons to whom the Software  #####
#####   is furnished to do so, subject to the following           #####
#####   conditions:                                               #####
#####                                                             #####
#####   Redistributions of source code must retain the above      #####
#####   copyright notice, this list of conditions and the         #####
#####   following disclaimers. Software changes, modifications,   #####
#####   or derivative works, should be noted with comments and    #####
#####   the author and organization's name.                       #####
#####                                                             #####
#####   Redistributions in binary form must reproduce the above   #####
#####   copyright notice, this list of conditions and the         #####
#####   following disclaimers in the documentation and/or other   #####
#####   materials provided with the distribution.                 #####
#####                                                             #####
#####   Neither the names of RDCEP, nor the Computation           #####
#####   Institute, the University of Chicago, UChicago Argonne    #####
#####   LLC, Argonne National Laboratory, the U.S. Government     #####
#####   nor the names of their contributors may be used to        #####
#####   endorse or promote products derived from this Software    #####
#####   without specific prior written permission.                #####
#####                                                             #####
#####   The software and the end-user documentation included      #####
#####   with the redistribution, if any, must include the         #####
#####   following acknowledgment: "This product includes          #####
#####   software produced by the Center for Robust                #####
#####   Decision-making on Climate and Energy Policy (RDCEP)."    #####
#####                                                             #####
#####   *********************************************             #####
#####   DISCLAIMER                                                #####
#####                                                             #####
#####   THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF     #####
#####   ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED   #####
#####   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       #####
#####   PARTICULAR PURPOSE AND NONINFRINGEMENT.                   #####
#####   NEITHER RDCEP, NOR THE UNIVERSITY OF CHICAGO, NOR THE     #####
#####   UNITED STATES GOVERNMENT, NOR THE UNITED STATES           #####
#####   DEPARTMENT OF ENERGY, NOR UCHICAGO ARGONNE, LLC, NOR      #####
#####   ANY OF THEIR EMPLOYEES, MAKES ANY WARRANTY, EXPRESS OR    #####
#####   IMPLIED, OR ASSUMES ANY LEGAL LIABILITY OR                #####
#####   RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, OR         #####
#####   USEFULNESS OF ANY INFORMATION, DATA, APPARATUS, PRODUCT,  #####
#####   OR PROCESS DISCLOSED, OR REPRESENTS THAT ITS USE WOULD    #####
#####   NOT INFRINGE PRIVATELY OWNED RIGHTS. IN NO EVENT SHALL    #####
#####   THE CONTRIBUTORS OR COPYRIGHT HOLDERS, BE LIABLE FOR ANY  #####
#####   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   #####
#####   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR   #####
#####   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       #####
#####   DEALINGS WITH THE SOFTWARE.                               #####
#####                                                             #####
#######################################################################
#######################################################################



use_packages("plyr")

#' fit_OLS
#'
#' Creates intial estimates for the parameters rho, beta0, and beta1 by minimizing
#' the Residual Sum of Squares (RSS) to the model data. The estimate for rho is used
#' as the initial value in the Autoregressive portion of the fit in fir_AR_w_op.
#'
#' Recall that the emulation equation is 
#'    Temperature(t) = b0 + b1 * (1-p)\sum_{k=0}^{t-1}(p^k*log(CO2-CO2_preindustrial)).
#' The function get_yX_matrix calculates the (1-p)\sum... portion of the equation
#' for a given value of p.
#' We will write X(t) = (1-p)\sum_{k=0}^{t-1}(p^k*log(CO2-CO2_preindustrial)). 
#' We will call X(t) the decay term. The emulation equation is then 
#'    Temperature(t) = b0 + b1 * X(t)
#' which is a linear function can can be fit by 
#' ordinary least squares (OLS). This function fixes a value of rho, calculates
#' the decay term, and performs the linear regression:
#'    Temperature ~ X
#' and gives the fitted parameters b0 (the intercept of the line)
#' and b1 (the slope of the line). fit_OLS does this for a number of possible 
#' values of rho and selects the one which minimized the residual sum of 
#' squares in the regression.  
#'
#' ('p' is rho)
#' 
#' @param single_m_r Dataframe containing the temperature and co2 data for a 
#' single region of a single model. Can contain multiple scenarios and realizations.
#' It should have the following columns, as per the output of region_aggregation:
#'     * time: the year of data in this row
#'     * co2: the co2 value for the given year
#'     * data: the data value extracted from the ncdf for the given year
#'     * region: the number of the region for this row
#'     * model: the model that produced this run
#'     * scenario: the name of the co2 scenario that produced this run
#'     * realization: the number of the realization of this run
fit_OLS <- function(single_m_r) 
{    
    #single model single region 
    rhos = seq(.01,.99,.01); nrho = length(rhos);

    #set up entry for RSS table
    ll = matrix(0, nrow = nrho, ncol = 6); 
    colnames(ll)=c("RSS", paste("beta",0:2,sep=""), "rho", "sig2")

    region = single_m_r[1,"region"]

    #loop through each possible value of rho and fit beta0 and beta1 assuming
    #that rho value is true. Calculate the RSS. Choose the parameter values 
    #that minimized the RSS.
    for (k in 1:nrho) 
    {
        #get rho
        rho = rhos[k]

        #now that we have a fixed value of rho,
        #construct the yX matrix, which is a matrix whose columns are
        #   temperature(t) | 1 | X(t)
        #including a column of just 1's gives us the intercept as a
        #parameter
        yX = get_yX_matrix(single_m_r, rho)

        #conduct a linear regression of temperature ~ X
        #this gives estimates for beta1 and beta0
        lm1 <- lm(yX[,1]~-1+yX[,c(-1)])
        b <- c(coef(lm1))

        #put parameter values and RSS into a row in a matrix
        ll[k,]=c(sum(lm1$residuals^2), b[1], b[2], b[3], rho, summary(lm1)$sigma^2)
    }

    #choose the row of the matrix that minimizes RSS
    ll[which.min(ll[,"RSS"]),][-1]    
}


#' get_yX_matrix
#'
#' Constructs the yX matrix. The yX matrix is a matrix which relates temperature
#' at a given time to the (1-p)\sum_{k=0}^{t-1}(p^k*log(CO2-CO2_preindustrial))
#' in the emulation equation. See fit_OLS. 
#' 
#' @param single_m_r Dataframe containing the temperature and co2 data for a 
#' single region of a single model. Can contain multiple scenarios and realizations.
#' It should have the following columns, as per the output of region_aggregation:
#'     * time: the year of data in this row
#'     * co2: the co2 value for the given year
#'     * temp: the temperature for this year
#'     * region: the number of the region for this row
#'     * model: the model that produced this run
#'     * scenario: the name of the co2 scenario that produced this run
#'     * realization: the number of the realization of this run
#' @param rho Our guess for the value of rho for this call.
get_yX_matrix <- function(single_m_r, rho) 
{

    #get the list of all co2 scenarios and realizations in this set of data 
    sc = unique(single_m_r$scenario)
    rl = unique(single_m_r$realization)

    #roundabout way to get the number of permutations of unique scenarios and
    #realizations and store it in n. We can't just multiply 
    #' length(sc) * length(rl) because some scenarios might have more
    #' realizations than other scenarios.
    nn <- table(single_m_r$scenario, single_m_r$realization)
    nlen = length(nn)
    n = max(nn)

    #create array to hold decay term parameters. The decay term is the
    #name given to (1-p)\sum_{k=0}^{t-1}(p^k*log(CO2-CO2_preindustrial)).
    DECAY <- array(0, dim = c(nlen, n))
    currow = 1;
    
    #loop through each combination of scenario and realization, get decay term parameter
    for(s in sc)
    {
        for(r in rl)
        {
            #get temperature data for single model, region, scenario, realization
            dat_m_r_s_r = subset(single_m_r, scenario == s & realization == r) 
            
            #get the decay term for the above data
            DECAY[currow, 1:nrow(dat_m_r_s_r)] = get_decay_term_single_scenario(dat_m_r_s_r$co2, rho)
            currow = currow + 1; 
        }
    }
    
    yX <- NULL 
    currow = 1;
    
    #paste it all into a single matrix
    for(s in sc)
    {
    
        for(r in rl)
        {
            #single model, region, scenario, realization (mrsr)
            dat_m_r_s_r = subset(single_m_r, scenario == s & realization == r)
            nr = nrow(dat_m_r_s_r)
            
            yX_c = cbind(dat_m_r_s_r$temp, 1, DECAY[currow, 1:nr])
            yX <- rbind(yX, yX_c)
            
            currow = currow + 1; 
    
        }
    }
    
    yX
}


#' fit_AR_w_op
#'
#' Calculates the parameters for the autoregression portion of the 
#' emulator equation using the OLS estimate for rho. 
#'
#' The fitting method is similar to what is described in fit_OLS, only
#' now we take into account the AR(1) portion of the emulation equation.
#'
#' @param single_m_r Dataframe containing the temperature and co2 data for a 
#' single region of a single model. Can contain multiple scenarios and realizations.
#' It should have the following columns, as per the output of region_aggregation:
#'     * time: the year of data in this row
#'     * co2: the co2 value for the given year
#'     * data: the data value extracted from the ncdf for the given year
#'     * region: the number of the region for this row
#'     * model: the model that produced this run
#'     * scenario: the name of the co2 scenario that produced this run
#'     * realization: the number of the realization of this run
#' @param coef_m_r The initial parameter estimates from fit_OLS.
fit_AR_w_op <- function(single_m_r, coef_m_r) 
{   
    r = unique(single_m_r$region)
    m = unique(single_m_r$model)
    
    #find the row of the correct region and model name and get rho from that row
    rho = coef_m_r[coef_m_r$region == r, "rho"]
    
    #rho is bounded to be in [delta, 1-delta]
    delta = 1e-4

    # use optim and arima to find the optimal rho
    while (!exists("rr") & rho > 0) 
    {
        try(
            rr <- optim(rho, function(rho) { #internal function passed to optim
                yX = get_yX_matrix(single_m_r, rho)
                mod <- arima(yX[,1], order = c(1,0,0), xreg = yX[,c(-1)], include.mean = F)
                mod$sigma2 #    sum(resid(mod)^2)/n 
            }, method = "L-BFGS-B", lower = 0 + delta, upper = 1-delta, hessian = T)
            , silent = T) 
        rho = rho - .09 
    }
    
    #extract parameter values and return
    if(exists("rr"))
    {
        rho = rr$par 
        yX = get_yX_matrix(single_m_r, rho)
        mod <- arima(yX[,1], order = c(1,0,0), xreg = yX[,c(-1)] , include.mean = F)
        bb <- c(coef(mod)[-1]); 
        list(return_data = data.frame(rho = rho, phi = coef(mod)[1], beta0 = bb[1], beta1 = bb[2], sig2 = mod$sigma2, var_rho = rr$hessian[1,1], var_phi = mod$var.coef[1,1], code = rr$convergence, message = rr$message), model = mod)
    } else {
        list(return_data = data.frame(rho = -1, phi = -1, beta0 = -1, beta1 = -1, beta2 = -1, sig2 = -1, var_rho = -1, var_phi = -1, code = "", message = ""), model = NULL)
    }
}


#' get_decay_term_single_scenario
#'
#' Returns the decay term, which is the name given to the values for 
#' (1-p)\sum_{k=0}^{t-1}(p^k*log(CO2-CO2_preindustrial))
#' for a given p and given CO2 timeseries.  
#'
#' @param dat_co2 A dataframe containing co2 values for
#' the single_m_r_s_r for which the decay term is being calculated.
#' @param rho Our guess for the value of rho.
get_decay_term_single_scenario <- function(co2, rho) 
{

    r <- numeric()
    
    n = length(co2)
    
    for (i in 1:n) 
    {
         r[i] = sum((co2[i:1] - PICO2) * rho^(0:(i-1)))
    }
    
    r*(1-rho)
}


#' fit_with_training_set
#'
#' Calculates parameters that give the best fit to the training set data
#' using least squared residuals. 
#'
#' @param training_set_data A dataframe containing the data on temperature
#' as a function of co2. This dataframe should have the columns:
#'    * region: the number of the region for this row
#'    * time: year
#'    * co2: co2 value for that year
#'    * temp: temperature in K for that region in that year
#'    * scenario: the name of the co2 scenario used to produce this data
#' @param training_set_name A string used to identify the training set.
#' The user can choose anything descriptive, but the convention is to
#' append the names of all scenarios in the training_set with underscores.
#' @param model The name of the model used to produce these training set items.
fit_with_training_set <- function(training_set_data, training_set_name, model)
{
    #get the column names by which to split the data in ply calls.
    #if the data has both "region" and "region_name", we also want 
    #to get the names
    splits = c("region")
    if("region_name" %in% colnames(training_set_data))
        splits = c(splits, "region_name")

    #get parameter estimates without autoregressive part 
    #(to use as inital estimates in the AR part, I believe)
    parameters_only_decay = ddply(training_set_data, splits, fit_OLS)

    #fit using autoregressive part
    parameters = dlply(training_set_data, splits, fit_AR_w_op, 
        coef_m_r=parameters_only_decay)
    
    #extract parameters from list
    parameters = ldply(parameters, function(x) x$return_data)

    #append training set name
    training_set = training_set_name #make the column be named "training_set"
    parameters = cbind(parameters, model, training_set)

    parameters
}