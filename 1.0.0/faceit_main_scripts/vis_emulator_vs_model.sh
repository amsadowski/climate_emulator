#!/bin/bash
export R_LIBS=/mnt/galaxyTools/rlibs/
export CLIMATE_EMULATOR_REPO=/mnt/galaxyTools/climate_emulator
Rscript /mnt/galaxyTools/climate_emulator/1.0.0/src/visualization/emulator_vs_model_main.R $@